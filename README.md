# Environment installation

This folder contains optional configuration and installation files

Execute install.sh to install and configure my favorite packages

## Monitor

In order to have the same monitor configuration in both login screen and user session:

```sh
sudo install --owner=gdm --group=gdm $HOME/.config/monitors.xml /var/lib/gdm3/.config/monitors.xml
```
