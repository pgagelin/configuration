#!/usr/bin/env bash

#
# Personnal configuration files
#

# Forbid error and undefined variables
set -eu -o pipefail

cp bashrc "${HOME}"/.bashrc
cp gitconfig "${HOME}"/.gitconfig
cp vimrc "${HOME}"/.vimrc

mkdir -p "${HOME}"/.config/Code/User
cp keybindings.json "${HOME}"/.config/Code/User
cp settings.json "${HOME}"/.config/Code/User

echo -e "\033[01;32mOK\033[0m"
