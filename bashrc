#!/usr/bin/env bash

# If not running interactively, don't do anything
case $- in
    *i*)
        ;;
    *)
        return
        ;;
esac

source /usr/share/bash-completion/bash_completion

shopt -s histappend
HISTCONTROL=ignoreboth
HISTSIZE=1000
HISTFILESIZE=2000

GIT_PS1_SHOWDIRTYSTATE=1
PS1='\[\033[01;34m\]\w\[\033[00m\]\[\033[01;31m\]$(__git_ps1)\[\033[00m\] \$ '

alias grep='grep --color=auto'
alias ls='ls --color=auto'
alias make='make --no-print-directory'
alias tree='tree -C'

export EDITOR=vim
export PYTHONDONTWRITEBYTECODE=true
